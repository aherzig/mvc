<?php
//used on the live server
define('URL', $_SERVER['DOCUMENT_ROOT'].'/');

//local dev path
//$project = 'MVC_v3';
//define('URL', $_SERVER['DOCUMENT_ROOT'].'/Web_PHP/'.$project.'/');

/*
makes paths simpler
ex: getting something from app while in libraries
    include URL . '/app/controllers/file.php';
    instead of include '../app/controllers/file.php'; or ../../app/controllers/file.php etc
*/
define('APP', URL . 'app/');
/*
same idea except specifically for the app folder since its accessed often
*/
?>