<?php
class Home extends Controller{
	public function __construct(){
    parent::__construct(get_class($this));
  }

  public function index(){
  	$this->view->render('home/index');
  }
}