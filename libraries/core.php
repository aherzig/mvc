<?php
class Core {
	function __construct(){
		$url = isset($_GET['url']) ? $_GET['url'] : NULL;
		$url = rtrim($url,'/');
		$url = explode('/',$url);	//creates usable array from the url

		if(empty($url[0])){
			require APP.'controllers/home.php';	//default path
			$controller = new Home();
			$controller->index();
			return false;
		}

		$file = APP.'controllers/'.$url[0].'.php';
		if(file_exists(strtolower($file))){
			require strtolower($file);				//loads controller called in url
		}else{
			//404
			require APP.'controllers/error.php';
			$controller = new Error();
			return false;
		}	
		$controller = new $url[0];			//creates controller object

		if(isset($url[1])){
			if(isset($url[2])){
				$controller->{$url[1]}($url[2]);	//calls controller->method(param)
			}else{
				$controller->{$url[1]}();					//calls controller->method()
			}
		}else{
			$controller->index();								//no method in url ? call index method
		}
	}
}