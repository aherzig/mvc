<?php
class View{
	function __construct(){

	}

	public function render($name){
		require APP.'views/header.php';
		require APP.'views/'.$name.'.php';
		require APP.'views/footer.php';
	}

	public function link_to($text, $link, $new_tab = false){
		echo "<a href='".$link."'";
    if($new_tab)
      echo " target='_blank'";
    echo ">".$text."</a>";
	}

	public function image_tag($string){
		echo "<img src='".APP."assets/images/".$image."'>";
	}

	public function stylesheet_link_tag($stylesheet){
		echo "<link type='text/css' rel='stylesheet' href='".APP."assets/stylesheets/".$stylesheet."' />";
	}

	public function javascript_include_tag($javascript){
		echo "<script type='text/javascript' src='".APP."assets/javascripts/".$javascript."' ></script>";
	}

	public function favicon_link_tag($favicon){
		echo "<link rel='shortcut icon' type='image/x-icon' href='".APP."assets/images/".$favicon."' />";
	}

	public function meta_tags(){
		echo "<meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no' />";
		echo "<meta name='viewport' content='width=device-width' />";
	}

	public function br2nl($string){
		$return = preg_replace('#<br/s*/?>#i'," ", $string);
		return $return;
	}
}