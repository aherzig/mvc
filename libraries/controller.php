<?php

class Controller {
	function __construct($model = NULL){
		$this->view = new View();
		if(isset($model)){
			$path = APP . 'models/' . $model . '_model.php';
			if(file_exists(strtolower($path))){
				require strtolower($path);
				$model_name = $model . '_Model';
				$this->model = new $model_name;
			}
		}
	}
}